def session_helper(request):
    session = dict()
    session['signed'] = request.session.get('signed')

    if request.session.get('username') is not None:
        session['username'] = request.session.get('username')
    else:
        session['username'] = ''

    return session

def set_login_session(request, user):
    request.session['signed'] = True
    request.session['username'] = user.get_short_name()

    return request.session

def set_logout_session(request):
    request.session['signed'] = False
    request.session['username'] = ''

    return request.session