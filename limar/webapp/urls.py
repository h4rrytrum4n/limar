from django.conf.urls import url
from . import views

# Define URL patterns
# Each pattern triggers the corresponding view
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'index/', views.index, name='index'),
    url(r'signin/', views.sign_in, name='signin'),
    url(r'signup/', views.sign_up, name='signup'),
    url(r'signout/', views.sign_out, name='signout'),
    url(r'artists/', views.artists, name='artists'),
    url(r'releases/', views.releases, name='releases'),
    url(r'^release/(?P<rid>\d+)/$', views.release, name='release'),

]