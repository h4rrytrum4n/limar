from django import forms


class signInForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Username', 'autofocus' : 'true,'}), label='', required='true')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class' : 'form-control', 'placeholder' : 'Password', }), label='', required='true')

class signUpForm(forms.Form):
    firstname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'John', 'id': 'firstName'}), label='', required='true')
    lastname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Doe', 'id': 'lasttName'}), label='', required='true')
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder': 'Username', 'id': 'username'}), label='', required='true')
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder': 'Password', 'id': 'password'}), label='', required='true')
    email = forms.CharField(widget=forms.EmailInput(attrs={'class': 'form-control','placeholder': 'you@example.com', 'id': 'email'}), label='', required='true')