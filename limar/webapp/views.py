from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from .forms import signInForm
from .forms import signUpForm
from .session import session_helper
from .session import set_login_session
from .session import set_logout_session

# Create your views here.


# Triggered by url /index
def index(request):

    return render(request, "index.html", session_helper(request))


def sign_in(request):
    if request.POST:
        form = signInForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user = authenticate(username=data['username'], password=data['password'])
            if user is not None:
                set_login_session(request, user)
                return render(request, "index.html", session_helper(request))
            else:
                form.add_error(None, "Wrong username or password")
    else:
        form = signInForm()

    session = session_helper(request)
    return render(request, "signin.html", {'form': form, 'signed': session['signed'], 'username': session['username']})


def sign_up(request):
    if request.POST:
        form = signUpForm(request.POST)
        if form.is_valid():
            try:
                data = form.cleaned_data
                user = User.objects.create_user(data['username'], data['email'], data['password'], first_name=data['firstname']
                                         , last_name=data['lastname'])
                set_login_session(request, user)
            except Exception as ex:
                form.add_error(None, "Username already taken")
    else:
        form = signUpForm()

    session = session_helper(request)
    return render(request, "signup.html", {'form': form, 'signed': session['signed'], 'username': session['username'] })


def sign_out(request):
    set_logout_session(request)

    return render(request, "signout.html", session_helper(request))

def artists(request):

    return render(request, "artists.html", session_helper(request))

def releases(request):

    return render(request, "releases.html", session_helper(request))

def release(request):

    return render(request, "releases.html", session_helper(request))