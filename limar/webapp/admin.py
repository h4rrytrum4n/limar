from .models import Release
from .models import Track
from .models import Artist
from django.contrib import admin

# Register your models here.
admin.site.register(Release)
admin.site.register(Artist)
admin.site.register(Track)