import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


def setupdb():
    try:
        conn = psycopg2.connect("dbname='limar' user='dbadmin' host='localhost' password='a6nq4i'")
        print("Connection sucessful")
    except Exception as ex:
        print(ex)
        return

    # Create a cursor which will be used for DB programming
    cur = conn.cursor()

    try:
        # Execute query and commit changes
        cur.execute("CREATE USER dbadmin WITH PASSWORD 'a6nq4i' CREATEDB;")
        conn.commit()
    except Exception as ex:
        print(ex)
    try:
        # Execute query and commit changes
        cur.execute("""CREATE DATABASE limar;""")
        conn.commit()
    except Exception as ex:
        print(ex)

    # Close connection
    cur.close()
    conn.close()


def setuptables():
    try:
        conn = psycopg2.connect("dbname='limar' user='dbadmin' host='localhost' password='a6nq4i'")
        print("Connection sucessful")
    except Exception as ex:
        print(ex)
        return

    cur = conn.cursor()

    try:
        # Execute query and commit changes
        cur.execute(
            """CREATE TABLE artist (aid serial primary key, name varchar(30), description varchar(2048), fotourl varchar(2083));""")
        conn.commit()
    except Exception as ex:
        print(ex)
    try:
        # Execute query and commit changes
        cur.execute(
            """CREATE TABLE release (rid serial primary key, catnr varchar(12), releasedate date, tracksnr int, coverurl varchar(2083));""")
        conn.commit()
    except Exception as ex:
        print(ex)
    try:
        # Execute query and commit changes
        cur.execute(
            """CREATE TABLE track (tid serial primary key, trackurl varchar(2083), artistid int references artist(aid), releaseid int references release(rid));""")
        conn.commit()
    except Exception as ex:
        print(ex)

    # Close connection
    cur.close()
    conn.close()

    print("All done!")


if __name__ == "__main__":
    setupdb()
    setuptables()
