from django.contrib import admin
from limar.webapp.models import Release
from limar.webapp.models import Track
from  limar.webapp.models import Artist

# Register your models here.
admin.site.register(Release)
admin.site.register(Artist)
admin.site.register(Track)