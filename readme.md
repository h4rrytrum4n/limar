# Quick install guide

## Getting all dependencies
1. Install python3 on mac `brew install python3`
2. Install postgresql `brew install postgresql`
3. `pip3 install virtualenv`
4. Create virtual enviroment `virtualenv venv` and activate it 'source venv/bin/activate'
5. Install dependencies `pip3 install Django` `pip3 install psycopg2`
6. Create new project `django-admin startproject limar`
7. Create a new app inside the project 'python3 manage.py startapp webapp'
8. Start web server on  `localhost:8000` `python manage.py runserver`

## Setting up the database
1. Install PostgreSQL `brew install postgresql`
2. Start PostgreSQL as a service `brew services start postgresql`
3. Execute `python3 limar/setupDB.py`


## Initialize the django webserver
1. Make the necessary migrations `python3 manage.py migrate`
2. Run server on localhost `python3 manage.py runserver`



